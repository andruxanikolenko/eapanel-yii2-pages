<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eapanel\pages\models;

use Yii;
use eapanel\pages\Module;

/**
 * Description of ContactForm
 *
 * @author Tartharia
 */
class CallbackForm extends \yii\base\Model{
    public $name;
    
    public $phone;
    
    public $time;
    
    public function attributeLabels() {
        return[
            'name'=>Module::t('app', 'Your name'),
            'phone'=>Module::t('app', 'Phone number'),
            'time'=>Module::t('app', 'Preferred time')
        ];
    }
    
    public function rules()
    {
        return [
            [['name','phone'],'required']
        ];
    }
    
    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom(['noreply@admin.ras' => 'Robot'])
                ->setSubject($this->setSubject())
                ->setTextBody($this->setBody())
                ->send();

            return true;
        } else {
            return false;
        }
    }
    
    protected function setBody()
    {
        return "Имя:$this->name<br>{$this->phone}<br>{$this->time}";
    }
    
    protected function setSubject()
    {
        return 'Запрос обратного звонка';
    }
            
}
