<?php
return[
    'News'=>'Новости',
    'Articles'=>'Статьи',
    'Menu'=>'Меню',
    'Pages'=>'Страницы',
    'Inactive'=>'Неактивные',
    'System Pages'=>'Системные страницы',
    'Create Page'=>'Создать страницу',
    'Create'=>'Добавить',
    'Create news'=>'Добавить новость',
    'Create article'=>'Добавить статью',
    'News title'=>'Заголовок новости',
    'Article title'=>'Заголовок статьи',
    'Save Menu'=>'Сохранить меню',
    'Save Changes'=>'Сохранить изменения',
    'Go to page'=>'Перейти на страницу',
    'Update Page'=>'Обновить страницу',
    'Edit Page'=>'Редактировать страницу',
    'Delete Page'=>'Удалить страницу',
    'Restore Page'=>'Восстановить страницу',
    'Your name'=>'Ваше имя',
    'Phone number'=>'Номер телефона',
    'Preferred time' => 'Удобное время звонка',
    'Cancel' => 'Отмена',
    'Send' =>'Отправить'    
    
];