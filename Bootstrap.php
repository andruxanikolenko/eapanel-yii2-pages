<?php

namespace eapanel\pages;

use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;
use yii\web\GroupUrlRule;

/**
 * Class Bootstrap
 */
class Bootstrap implements BootstrapInterface
{

    public function bootstrap($app)
    {
        /* @var $module \eapanel\pages\Module */
        if ($app->hasModule('pages')) {
            
            $module = $app->getModule('pages');
            
            $modelMap = ArrayHelper::merge($this->_modelMap(), $module->modelMap);
            foreach ($modelMap as $name => $definition) {
                $class = "{$module->modelNamespace}\{$name}";
                \Yii::$container->set($class, $definition);
            }
            
            $configUrlRule = [
                'rules'  => $module->urlRules
            ];
    
            $app->get('urlManager')->rules[] = new GroupUrlRule($configUrlRule);
        }
    }
    
    private function _modelMap()
    {
        return [
            'ContactForm'=> models\ContactForm::className(),
            'CallbackForm'=>  models\CallbackForm::className()
        ];
    }
}