<?php

use eapanel\pages\Module;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $model eapanel\pages\models\CallbackForm */
/* @var $form yii\widgets\ActiveForm */
?>
<?php Pjax::begin([
    'id'=>'pjax-callback-form',
    'enablePushState'=>false
])?>

<div class="view-callback-form">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <span class="icon"></span>
    <?=  Html::tag('h3',  Yii::t('app_labels', 'Request a call back'))?>
    <?php $form =  ActiveForm::begin([
        'id'=>'callback-form',
        'action'=>Url::to('/pages/main/callback'),
        'options'=>['data-pjax'=>'1'],
        'fieldConfig'=>[
            'template'=>"<div class='row'><div class='col-xs-3'>{label}</div><div class='col-xs-9'>{input}\n{error}</div></div>"
        ]
    ])?>
        <?=$form->field($model,'name')->textInput()?>
        <?=$form->field($model,'phone')->textInput()?>
        <?=$form->field($model,'time')->textInput()?>
    <div class="row buttons">
        <?=Html::button(Module::t('app','Cancel'),['data-dismiss'=>'modal'])?>
        <?=Html::submitButton(Module::t('app', 'Send'));?>
    </div>
    <?php $form->end()?>
</div>
<?php Pjax::end()?>