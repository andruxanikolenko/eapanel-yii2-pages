<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
    use eapanel\pages\models as models;
    use yii\bootstrap\ActiveForm;
    use kartik\sortinput\SortableInput;
    use yii\widgets\Pjax;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
?>
<div class="col span_4_of_12 systemPages">
    <h1><?= eapanel\pages\Module::t('app', 'System Pages'); ?></h1>
    <?php
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            ['label' => eapanel\pages\Module::t('app', 'Create Page'), 'class'=>'createPageLink', 'url' => ['/pages/admin/create'], 'linkOptions' => ['class'=>'createPageLink']],
        ],
    ]);
    ?>
    <h1><?= eapanel\pages\Module::t('app', 'Inactive'); ?></h1>
    <?php Pjax::begin(['id'=>'inactivePagesContainer']) ?>
    <?php
    echo SortableInput::widget([
        'name'=>'pages',
        'items' => $inactiveItems,
        'hideInput' => true,
        'sortableOptions' => [
            'connected'=>true,
        ],
        'options' => ['class'=>'form-control', 'readonly'=>true]
    ]);
    ?>
    <?php Pjax::end() ?>
</div>
<div class="col span_4_of_12 allPages">
    <h1><?= eapanel\pages\Module::t('app', 'Pages'); ?></h1>
    <?php Pjax::begin(['id'=>'allPagesContainer']) ?>
    <?php
        echo SortableInput::widget([
            'name'=>'pages',
            'items' => $items,
            'hideInput' => true,
            'sortableOptions' => [
                'connected'=>true,
            ],
            'options' => ['class'=>'form-control', 'readonly'=>true]
        ]);
    ?>
    <?php Pjax::end() ?>
</div>
<!--div class="col span_4_of_12 menuPages">
    <h1><?= eapanel\pages\Module::t('app', 'Menu'); ?></h1>
    <!--
    <?php// Pjax::begin(['id'=>'menuContainer']) ?>
    <?php/*
        echo SortableInput::widget([
            'name'=>'menu',
            'id'=>'menu',
            'items' => $menuItems,
            'hideInput' => true,
            'sortableOptions' => [
                'itemOptions'=>['class'=>'alert alert-warning dropMenu'],
                'connected'=>true,
            ],
            'options' => ['class'=>'form-control', 'readonly'=>true]
        ]);*/
    ?>
    <?php// Pjax::end()?>
    <button id="saveMenu" class="btn btn-primary"><?= eapanel\pages\Module::t('app', 'Save Menu'); ?></button>
</div-->
<div class="col span_12_of_12 formContainer" id="formContainer"></div>