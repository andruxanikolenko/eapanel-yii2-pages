<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Inactive';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the inactive page. It will not be displayed on the frontend. You can make it active or completely remove
    </p>

    <code><?= __FILE__ ?></code>
</div>
