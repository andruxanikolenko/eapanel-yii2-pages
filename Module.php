<?php
/**
 * Модуль иерархии страниц сайта
 * 
 * @package pages
 */

namespace eapanel\pages;

use Yii;
use yii\filters\AccessControl;
use eapanel\pages\ModuleAssets;

/**
 * Класс модуля страниц
 * 
 * Добавление просмотр и редактирование статических страниц.
 * @property string $controllerNamespace
 */
 
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'eapanel\pages\controllers';
    
    public $modelNamespace = 'eapanel\pages\models';

    public $defaultRoute = 'main';
    
    /** @var array Model map */
    public $modelMap = [];
    
    /** @var array The rules to be used in URL management. */
    public $urlRules = [
        'pages'                             => 'pages/admin/index', //Панель управления страницами
        'page/create'                       => 'pages/admin/create', //Создать новую страницу
        'page/<_a:(update)>/<id:[\w\-]+>'   => 'pages/admin/update', //Редактировать страницу
        'page/<_a:(delete)>/<id:[\w\-]+>'   => 'pages/admin/delete', //Удалить страницу
        'page/<_a:(remove)>/<id:[\w\-]+>'   => 'pages/admin/remove', //Удалить страницу навсегда
        'page/<_a:(restore)>/<id:[\w\-]+>'  => 'pages/admin/restore', //Восстановить страницу
        'page/<id:[\w\-]+>' => 'pages/main/view',
    ];
    
    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'controllers'=>["{$this->id}/main"]
                    ],
                    [
                        'allow' => true,
                        'roles' => ['Administrator'],
                        'controllers'=>["{$this->id}/admin"]
                    ]
                ],
            ],
        ];
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['pages/*'] = [
            'class'          => 'yii\i18n\PhpMessageSource',
            'basePath'       => '@eapanel/pages/messages',
            'fileMap'        => [
                'pages/app' => 'app.php'
            ],
        ];
    }

    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('pages/' . $category, $message, $params, $language);
    }
}
