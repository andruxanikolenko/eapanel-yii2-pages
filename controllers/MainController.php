<?php

namespace eapanel\pages\controllers;

use Yii;
use yii\web\HttpException;
use eapanel\pages\models as models;
use yii\helpers\Inflector;
use dosamigos\transliterator\TransliteratorHelper;

use howard\behaviors\iwb\InlineWidgetsBehavior;

class MainController extends \yii\web\Controller
{
    use \eapanel\pages\components\AjaxValidationTrait;
    
    private $viewsPath = '@app/themes/basic/views/yii2-pages/main';
    
    public function behaviors()
    {
        return [
            'InlineWidgetsBehavior'=> [
                'class'=> InlineWidgetsBehavior::className(),
                //'namespace'=> 'eapanel\pages\components',               
                //'widgets'=>Yii::$app->params['runtimeWidgets'],
                'startBlock'=> '[*',
                'endBlock'=> '*]',
                'classSuffix'=> 'Widget',
             ],
        ];
    }
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/images/',
                'path' => '@webroot/images'
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/images/',
                'path' => '@webroot/images',
                'type' => \vova07\imperavi\actions\GetAction::TYPE_IMAGES,
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/files/',
                'path' => '@webroot/files',
                'uploadOnlyImage'=>false
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => '/files/',
                'path' => '@webroot/files',
                'type' => \vova07\imperavi\actions\GetAction::TYPE_FILES,
            ]
        ];
    }
    
    public function actionIndex()
    {        
        return $this->render('index');
    }
    
    public function actionView()
    {
        $page = new models\Page(['filename'=> \Yii::$app->request->get('id')]);
        //$title = $page->getPageName();
        //$content = $this->decodeWidgets($page->getPageContent());
        
        
        if(!$page->exists())
        {
            throw new HttpException(404,  \Yii::t('messages','Page not found'));
        }
        return $this->render($this->viewsPath . '/' . $page->filename, [
            //'title'   => $title,
            //'content' => $content
        ]);
    }    
    
    public function actionCallback()
    {
        $model = Yii::createObject(models\CallbackForm::className());
        
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('formSubmitted');
            return $this->redirect('/');
        } else {
            $params = [
                'model' => $model,
            ];
            return Yii::$app->request->isAjax?$this->renderAjax('callback.php', $params):$this->render('callback',$params);
        }
    }
    
}